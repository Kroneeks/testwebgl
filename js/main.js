main();

function main() {
   const gl = document.querySelector("#gl").getContext("webgl");

   function createShader(type, source) {
       const shader = gl.createShader(type);
       gl.shaderSource(shader, source);
       gl.compileShader(shader);
       if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
           alert('Shader compilation error: ' + gl.getShaderInfoLog(shader));
           return null;
       }
       return shader;
    }

    function createProgram(vsrc, fsrc) {
        const vshader = createShader(gl.VERTEX_SHADER, vsrc);
        const fshader = createShader(gl.FRAGMENT_SHADER, fsrc);

        const program = gl.createProgram();
        gl.attachShader(program, vshader);
        gl.attachShader(program, fshader);
        gl.linkProgram(program);

        if (!gl.getProgramParameter(program, gl.LINK_STATUS)){
            alert('Program error: ' + gl.getProgramInfoLog(program));
            return null;
        }

        return program;
    }

    function mat4Perspective(fovy, aspect, near, far) {
        let f = 1.0 / Math.tan(fovy / 2.0);
        let nf = 1.0 / (near - far);
        return new Float32Array([
            f / aspect, 0,                   0,  0,
            0,          f,                   0,  0,
            0,          0,   (far + near) * nf, -1,
            0,          0, 2 * far * near * nf,  0
        ]);
    }
    
    /*
    function mat4RY(a) {
        const c = Math.cos(a), 
              s = Math.sin(a);
        return new Float32Array([
             c, 0, s, 0,
             0, 1, 0, 0,
            -s, 0, c, 0,
             0, 0, 0, 1 
        ]);
    }
    */

    function mat4LookAt(cam, at, up){
        let zx = cam[0] - at[0],
            zy = cam[1] - at[1],
            zz = cam[2] - at[2];
        const zrmag = 1.0 / Math.sqrt(zx * zx + zy * zy + zz * zz);
        zx *= zrmag; zy *= zrmag; zz *= zrmag;

        let xx = up[1] * zz - up[2] * zy,
            xy = up[2] * zx - up[0] * zz,
            xz = up[0] * zy - up[1] * zx;
        const xrmag = 1.0 / Math.sqrt(xx * xx + xy * xy + xz * xz);
        xx *= xrmag; xy *= xrmag; xz *= xrmag;

        let yx = zy * xz - zz * xy,
            yy = zz * xx - zx * xz,
            yz = zx * xy - zy * xx;
        const yrmag = 1.0 / Math.sqrt(yx * yx + yy * yy + yz * yz);
        yx *= yrmag; yy *= yrmag; yz *= yrmag;

        return new Float32Array([
            xx, yx, zx, 0,
            xy, yy, zy, 0, 
            xz, yz, zz, 0, 
            -(xx * cam[0] + xy * cam[1] + xz * cam[2]),
            -(yx * cam[0] + yy * cam[1] + yz * cam[2]),
            -(zx * cam[0] + zy * cam[1] + zz * cam[2]),
            1
        ]);
    }

    function loadTexture(url) {
        const tex = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, tex);

        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 255, 255, 255]));
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

        const image = new Image();
        image.onload = function() {
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
            gl.generateMipmap(gl.TEXTURE_2D);
        }
        image.src = url;
        return tex;
    }

    const tex = loadTexture('../img/earth.jpg'); 

    const prog = createProgram(
        document.getElementById('vsh').textContent,
        document.getElementById('fsh').textContent);
    gl.useProgram(prog);
    const pos_loc = gl.getAttribLocation(prog, 'pos');
    const uv_loc = gl.getAttribLocation(prog, 'uv');

    /*
    const quad_vtx = new Float32Array(
        [-1, 1, -1, -1, 1, 1, 1, -1]
    );
    */

    const M = 16, N = 16;
    const sph_vn = ((M + 1) * (N + 1));
    let sph_vtx = new Float32Array(3 * sph_vn);
    let sph_vtx_uv = new Float32Array(2 * sph_vn);
    for (let i = 0; i < M + 1; ++i){
        const phi = i * Math.PI / M;
        const y = Math.cos(phi);
        const r = Math.sin(phi);
        for (let j = 0; j < N + 1; ++j){
            const th = j * 2.0 * Math.PI / N;
            const idx = (j + i * (N + 1)) * 3;
            sph_vtx[idx] = r * Math.cos(th);
            sph_vtx[idx+1] = y;
            sph_vtx[idx+2] = r * Math.sin(th);
            
            const uvidx = (j + i * (N + 1)) * 2;
            sph_vtx_uv[uvidx] =  1 - j / (N + 1); 
            sph_vtx_uv[uvidx+1] = i / (M + 1);
        }
    }

    const buf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buf);
    gl.bufferData(gl.ARRAY_BUFFER, sph_vtx, gl.STATIC_DRAW);
    gl.vertexAttribPointer(pos_loc, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(pos_loc);

    const uvbuf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, uvbuf);
    gl.bufferData(gl.ARRAY_BUFFER, sph_vtx_uv, gl.STATIC_DRAW);
    gl.vertexAttribPointer(uv_loc, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(uv_loc);

    let sph_in = M * (N * 2 + 2) + (M - 1) * 2;
    const sph_idx = new Uint16Array(sph_in);
    let idx = 0;
    for(let i = 0; i < M; ++i){
        for(let j = 0; j < (N + 1); ++j){
            sph_idx[idx++] = i * (N + 1) + j;
            sph_idx[idx++] = (i + 1) * (N + 1) + j;
        }

        // strip restart
        if(i < M - 1){
            sph_idx[idx++] = (i + 1) * (N + 1) + N;  
            sph_idx[idx++] = (i + 1) * (N + 1);   
        }  
    }

    const bufi = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bufi);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, sph_idx, gl.STATIC_DRAW);

    let mat_proj = null;

    function resize() {
        const width = gl.canvas.clientWidth,
              height = gl.canvas.clientHeight;
        if(gl.canvas.width != width || gl.canvas.height != height){
            gl.canvas.width = width;
            gl.canvas.height = height; 
            mat_proj = mat4Perspective(Math.PI / 3., width / height, 0.1, 100.0);
            gl.viewport(0, 0, width, height); 
        }
    }

    gl.cullFace(gl.FRONT);
    gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);

    function render() {
        resize();

        const t = Date.now() / 1000;

        const c = Math.cos(t), s = Math.sin(t);
        const mat_view = mat4LookAt([2*s, 1, 2*c], [0, 0, 0], [0, 1, 0]);
        gl.uniformMatrix4fv(gl.getUniformLocation(prog, 'mview'), false, mat_view);
        gl.uniformMatrix4fv(gl.getUniformLocation(prog, 'mproj'), false, mat_proj);
        gl.uniform1i(gl.getUniformLocation(prog, 'tex'), 0);

        gl.clearColor(0,0,0,1);
        gl.clear(gl.COLOR_BUFFER_BIT);

        gl.drawElements(gl.TRIANGLE_STRIP, sph_in, gl.UNSIGNED_SHORT, 0);
        requestAnimationFrame(render);
    }
    render();
}
